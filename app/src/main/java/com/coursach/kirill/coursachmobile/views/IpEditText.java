package com.coursach.kirill.coursachmobile.views;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.widget.EditText;


public class IpEditText extends AppCompatEditText {
    public IpEditText(Context context) {
        super(context);
    }

    public IpEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public IpEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }




}
