package com.coursach.kirill.coursachmobile;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.IOException;

public class MainActivity extends Activity {
    
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_CAMERA_PERMISSION_CODE = 1;

    private SurfaceView mSurfaceView;
    private CameraSource mCameraSource;

    private boolean mSurfaceCreated;
    private boolean mPreviewRequested;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSurfaceView = (SurfaceView) findViewById(R.id.surface_view);
        mSurfaceView.getHolder().addCallback(new SurfaceCallback());
        createCameraSource();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPreviewRequested = true;
        startPreviewIfReady();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCameraSource.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCameraSource.release();
        mCameraSource = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startPreviewIfReady();
            } else
                stopActivityAndShowToast("Нужны права на камеру.");
        } else super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void createCameraSource() {
        Context context = getApplicationContext();
        TextRecognizer textRecognizer = new TextRecognizer.Builder(context).build();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        mCameraSource = new CameraSource.Builder(context, textRecognizer)
                .setAutoFocusEnabled(true)
                .setRequestedFps(30)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(displayMetrics.heightPixels, displayMetrics.widthPixels)
                .build();
        
    }

    private void startPreviewIfReady(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION_CODE);
            return;
        }
        if (!mSurfaceCreated) return;

        if (!mPreviewRequested) return;

        try {
            mCameraSource.start(mSurfaceView.getHolder());

            mPreviewRequested = false;
        } catch (IOException e) {
            e.printStackTrace();
            stopActivityAndShowToast(
                    "Проблемы с открытием камеры. \n Попробуйте перезапустить приложение.");
        }
    }

    private void stopActivityAndShowToast(CharSequence text){
        Toast.makeText(getApplicationContext(),
                text,
                Toast.LENGTH_LONG)
                .show();
        finishActivity(0);
    }

    private class SurfaceCallback implements SurfaceHolder.Callback{

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            mSurfaceCreated = true;
            startPreviewIfReady();
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            mSurfaceCreated = false;
        }
    }
}
